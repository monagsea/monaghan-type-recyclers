package com.example.typelistrecyclerviewer.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.typelistrecyclerviewer.model.ListsRepo
import kotlinx.coroutines.launch

class ListsViewModel: ViewModel() {
    private val repo = ListsRepo

    private val _stringList = MutableLiveData<List<String>>()
    val stringList: LiveData<List<String>> get() = _stringList

    private val _intList = MutableLiveData<List<Int>>()
    val intList: LiveData<List<Int>> get() = _intList

    private val _doubleList = MutableLiveData<List<Double>>()
    val doubleList: LiveData<List<Double>> get() = _doubleList

    private val _categoryList = MutableLiveData<List<String>>()
    val categoryList: LiveData<List<String>> get() = _categoryList

    fun getCategories() {
        viewModelScope.launch {
            val categoryList = repo.getCategories()
            _categoryList.value = categoryList
        }
    }

    fun getStrings() {
        viewModelScope.launch {
            val stringList = repo.getStrings()
            _stringList.value = stringList
        }
    }

    fun getInts() {
        viewModelScope.launch {
            val intList = repo.getInts()
            _intList.value = intList
        }
    }

    fun getDoubles() {
        viewModelScope.launch {
            val doubleList = repo.getDoubles()
            _doubleList.value = doubleList
        }
    }
}