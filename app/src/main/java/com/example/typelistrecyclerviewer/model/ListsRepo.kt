package com.example.typelistrecyclerviewer.model

import com.example.typelistrecyclerviewer.model.remote.ListsApi

object ListsRepo: ListsApi {

    override suspend fun getCategories(): List<String> {
        return listOf("Strings", "Ints", "Doubles")
    }

    override suspend fun getStrings(): List<String> {
        return listOf(
            "hello", "this", "is", "a", "hardcoded", "list", "of", "Strings", "for", "your", "viewing", "pleasure"
        )
    }

    override suspend fun getInts(): List<Int> {
        return listOf(1,2,3,4,5,6,7,8,9,10)
    }

    override suspend fun getDoubles(): List<Double> {
        return listOf(1.1,2.2,3.3,4.4,5.5,6.6,7.7,8.8,9.9,10.10)
    }
}