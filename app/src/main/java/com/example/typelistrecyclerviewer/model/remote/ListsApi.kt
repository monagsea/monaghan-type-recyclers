package com.example.typelistrecyclerviewer.model.remote

interface ListsApi {
    suspend fun getCategories(): List<String>
    suspend fun getStrings(): List<String>
    suspend fun getInts(): List<Int>
    suspend fun getDoubles(): List<Double>
}