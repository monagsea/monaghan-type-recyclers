package com.example.typelistrecyclerviewer.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.typelistrecyclerviewer.Recycler.IntegerAdapter
import com.example.typelistrecyclerviewer.databinding.FragmentIntListBinding
import com.example.typelistrecyclerviewer.viewmodel.ListsViewModel


class IntListFragment : Fragment() {
    private var _binding: FragmentIntListBinding? = null
    private val binding get() = _binding!!
    private val listsViewModel by viewModels<ListsViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentIntListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun displayIntegers() =
            with(binding.rvList) {
                layoutManager =
                    LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
                adapter = IntegerAdapter().apply {
                    with(listsViewModel) {
                        getInts()
                        intList.observe(viewLifecycleOwner) {
                            addIntegers(it)
                        }
                    }
                }
            }
        displayIntegers()
    }
}