package com.example.typelistrecyclerviewer.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.HorizontalScrollView
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.typelistrecyclerviewer.R
import com.example.typelistrecyclerviewer.Recycler.CategoryAdapter
import com.example.typelistrecyclerviewer.databinding.FragmentFirstBinding
import com.example.typelistrecyclerviewer.viewmodel.ListsViewModel


class FirstFragment : Fragment() {
    private var _binding: FragmentFirstBinding? = null
    private val binding get() = _binding!!
    private val listsViewModel by viewModels<ListsViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFirstBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun displayCategories() =
            with(binding.rvList) {
                layoutManager =
                    StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                adapter = CategoryAdapter().apply {
                    with(listsViewModel) {
                        getCategories()
                        categoryList.observe(viewLifecycleOwner) {
                            addCategories(it)
                        }
                    }
                }
            }
        displayCategories()
    }

}