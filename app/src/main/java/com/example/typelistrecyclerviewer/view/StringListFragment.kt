package com.example.typelistrecyclerviewer.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.typelistrecyclerviewer.Recycler.IntegerAdapter
import com.example.typelistrecyclerviewer.Recycler.StringAdapter
import com.example.typelistrecyclerviewer.databinding.FragmentStringListBinding
import com.example.typelistrecyclerviewer.viewmodel.ListsViewModel


class StringListFragment : Fragment() {
    private var _binding: FragmentStringListBinding? = null
    private val binding get() = _binding!!
    private val listsViewModel by viewModels<ListsViewModel>()



    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentStringListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fun displayStrings() =
            with(binding.rvList) {
                layoutManager =
                    LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
                adapter = StringAdapter().apply {
                    with(listsViewModel) {
                        getStrings()
                        stringList.observe(viewLifecycleOwner) {
                            addStrings(it)
                        }
                    }
                }
            }
        displayStrings()
    }
}