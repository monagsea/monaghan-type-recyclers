package com.example.typelistrecyclerviewer.Recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.typelistrecyclerviewer.databinding.ItemDoublesBinding

class DoubleAdapter : RecyclerView.Adapter<DoubleAdapter.DoubleViewHolder>() {
    private var doubles = listOf<Double>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DoubleViewHolder {
        val binding = ItemDoublesBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return DoubleViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DoubleViewHolder, position: Int) {
        val double = doubles[position]
        holder.loadDisplay(double)
    }

    override fun getItemCount(): Int {
        return doubles.size
    }

    fun addDoubles(doubles: List<Double>) {
        this.doubles = doubles.toMutableList()
        notifyDataSetChanged()
    }

    class DoubleViewHolder(
        private val binding: ItemDoublesBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(double: Double) {
            binding.tvDoubles.text = double.toString()
        }
    }
}