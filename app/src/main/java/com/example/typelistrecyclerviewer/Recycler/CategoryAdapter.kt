package com.example.typelistrecyclerviewer.Recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.typelistrecyclerviewer.databinding.ItemCategoriesBinding
import com.example.typelistrecyclerviewer.view.FirstFragmentDirections

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    private var categories = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val binding = ItemCategoriesBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return CategoryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.loadDisplay(category)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    fun addCategories(categories: List<String>) {
        this.categories = categories.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: ItemCategoriesBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(category: String) {
            binding.tvCategory.text = category
            when (category) {
                "Strings" -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToStringListFragment())
                    }
                }
                "Ints" -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToIntListFragment())
                    }
                }
                else -> {
                    binding.tvCategory.setOnClickListener {
                        it.findNavController().navigate(FirstFragmentDirections.actionFirstFragmentToDoubleListFragment())
                    }
                }
            }
        }
    }
}