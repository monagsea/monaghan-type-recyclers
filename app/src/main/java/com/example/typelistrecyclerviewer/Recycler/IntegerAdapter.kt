package com.example.typelistrecyclerviewer.Recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.typelistrecyclerviewer.databinding.ItemIntegersBinding

class IntegerAdapter : RecyclerView.Adapter<IntegerAdapter.IntegerViewHolder>() {
    private var integers = listOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IntegerViewHolder {
        val binding = ItemIntegersBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return IntegerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: IntegerViewHolder, position: Int) {
        val integer = integers[position]
        holder.loadDisplay(integer)
    }

    override fun getItemCount(): Int {
        return integers.size
    }

    fun addIntegers(integers: List<Int>) {
        this.integers = integers.toMutableList()
        notifyDataSetChanged()
    }

    class IntegerViewHolder(
        private val binding: ItemIntegersBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(integer: Int) {
            binding.tvInteger.text = integer.toString()
        }
    }
}