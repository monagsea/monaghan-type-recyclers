package com.example.typelistrecyclerviewer.Recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.typelistrecyclerviewer.databinding.ItemStringsBinding

class StringAdapter : RecyclerView.Adapter<StringAdapter.StringViewHolder>() {
    private var strings = listOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StringViewHolder {
        val binding = ItemStringsBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return StringViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StringViewHolder, position: Int) {
        val string = strings[position]
        holder.loadDisplay(string)
    }

    override fun getItemCount(): Int {
        return strings.size
    }

    fun addStrings(strings: List<String>) {
        this.strings = strings.toMutableList()
        notifyDataSetChanged()
    }

    class StringViewHolder(
        private val binding: ItemStringsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadDisplay(string: String) {
            binding.tvString.text = string.toString()
        }
    }
}